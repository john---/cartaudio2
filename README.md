# Boombox

- Bluetooth and analog in
- Analog out
- Audio amplification
- DSP

# Design

## Parametric case model in FreeCAD

The enclosure size for each driver (length, width and depth) are derived from the required driver volume and the ratios for length and width dimensions.  The depth is derived from the required size to reach the required driver volume.  Parameters are in the FreeCAD spreadsheet.

## Interface

Interfcace elements are those required for analog out, volume, on/off, power in, analog in, bluetooth reset, bluetooth status, and future functions.

Bluetooth status is via LEDs located inside the enclosure.

All other controls are on the front panel.

Power is externally provided through DC barrel jack [insert url somewhere]

# Implementation Notes

The bulk of the case was assembled from 1/4" MDF.  The enclosure cover was 3D printed.  Also 3D printed:  Port tubes, speaker grills and mounting components.

The interface hardware was provided by the Parts Express cable kit

# Hardware

- [Dayton Audio DMA58-4 2" Drivers](https://www.parts-express.com/Dayton-Audio-DMA58-4-2-Dual-Magnet-Aluminum-Cone-Full-Range-Driver-4-Ohm-295-582)
- [Dayton Audio KABD-230](https://www.parts-express.com/Dayton-Audio-KABD-230-2-x-30W-Bluetooth-Amp-Board-with-DSP-325-106)
- [Dayton Audio Function Cables for KABD Series](https://www.parts-express.com/Dayton-Audio-KABD-SPF-Function-Cables-for-KAB-DSP250-325-117)

t-nuts and screws to mount drivers

# Working With MDF

MDF has a lot of benificial properties but it is a pain to finish.  In the end I used multiple coats of [primer url] and [paint url].  Sanding was done before each coat (400 grit).

# Future enhancements

Switch to configure analog out to be subwoofer out instead of full range

# Alternate Implementation

The case can be 3D printed.

# Control Panel Changes

- Increase how rigid it is
  - Status: Done (increased edge depth) (V2)
- 3D printed mounts for internal boards, LED and maybe wire management
- Better looking controls (knob, buttons, etc)
- Maybe shorten up some of the wires
- Hole size changes
  - The hole for volume control was not big enough
    - Status: Done (V2)
  - The holes for the 3mm nuts were not big enough
    - Status: Done (V2)
  - The holes in the blocks were not big enough
    - Status: Done (V2)
- remove: power on/off (jut disconnect battery to turn it off)
  - Status: Done (V2)
- 3D print a hex mesh instead of open areas (control panel and speaker covers)
  - Status: Done for control panel (V2)
- Replace analog out for Bluetooth dongle with internal Bluetooth dongle

See [notes](~documentation/portable_audio_idea_modular_golf_cart_bluetooth.md) for background and in progress research.

